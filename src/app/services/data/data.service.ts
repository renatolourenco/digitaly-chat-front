import { Injectable, Output, EventEmitter } from '@angular/core';

import { ConnectionService } from '../connection/connection.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  @Output()
  message = new EventEmitter<Object>();
  openChatWindow = new EventEmitter<Boolean>();
  updateScroll = new EventEmitter<boolean>();

  compToDestroy: any;
  context = null;
  dialogArray = [];

  constructor(private connectionService: ConnectionService) { }

  private setDialogMessage(msg: Object) {
    this.message.emit(msg)
    this.dialogArray.push(msg);
    localStorage.setItem('test', JSON.stringify(this.dialogArray));
    this.setUpdateScroll();
  }

  public cleanVariables() {
    this.context = null;
    this.dialogArray = [];
  }

  public destroyComp() {
    this.compToDestroy.destroy();
  }

  public sendMessageToWatson(msg: Object) {
    let watsonObj = {
      context: this.context,
      input: msg['user_text']
    }

    if (msg['user_text'] !== '') {
      this.setDialogMessage({ 'user': msg['user_text'] })
    }

    this.connectionService.watson(watsonObj).subscribe(res => {
      this.dialogArray[0] = { 'context': res['context'] };
      this.context = res['context'];
      if (res['output']['carouselImages']) {
        res['output']['generic'].forEach((obj, idx) => {
          if (obj['text'] === 'carousel') {
            res['output']['generic'][idx]['carouselImages'] = res['output']['carouselImages'];
          }
        });
      }
      this.setDialogMessage({ 'bot': res['output']['generic'] })
    })

  }

  public setCompToDestroy(comp: any) {
    this.compToDestroy = comp;
  }

  public setOpenChatWindow(open: Boolean) {
    this.openChatWindow.emit(open);
  }

  public setUpdateScroll() {
    this.updateScroll.emit(true);
  }

  public startDialog() {
    let savedData = [];
    savedData = JSON.parse(localStorage.getItem('test'));
    if (savedData) {
      savedData.forEach(dialog => {
        if (dialog['context']) {
          this.context = dialog['context'];
          this.dialogArray[0] = { 'context': dialog['context'] };
        } else {
          this.setDialogMessage(dialog);
        }
      });
    } else {
      this.sendMessageToWatson({ 'user_text': '' });
    }
  }

}
