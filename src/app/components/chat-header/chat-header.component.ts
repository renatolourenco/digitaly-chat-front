import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { DataService } from 'src/app/services/data/data.service';
import { ChatModalComponent } from '../chat-modal/chat-modal.component';

@Component({
  selector: 'dc-chat-header',
  templateUrl: './chat-header.component.html',
  styleUrls: ['./chat-header.component.css']
})
export class ChatHeaderComponent implements OnInit {

  public avatarPath: String;

  constructor(private dataService: DataService, private modalService: NgbModal) { }

  ngOnInit() {
    this.avatarPath = `../../../assets/img/bot_avatar.png`
  }

  public closeChatWindow() {
    this.dataService.cleanVariables();
    this.dataService.setOpenChatWindow(false);
  }

  public cleanDialog() {
    localStorage.removeItem('test');
    this.dataService.cleanVariables();
    this.dataService.setOpenChatWindow(false);
  }

  public open() {
    const modalRef = this.modalService.open(ChatModalComponent);
    modalRef.componentInstance.my_modal_title = 'I your title - Renato';
    modalRef.componentInstance.my_modal_content = 'I am your content - Lourenco';
  }

}
