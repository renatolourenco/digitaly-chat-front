import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'dc-carousel-balloon',
  templateUrl: './carousel-balloon.component.html',
  styleUrls: ['./carousel-balloon.component.css']
})
export class CarouselBalloonComponent implements OnInit {

  @ViewChild('carousel', { static: true }) carousel: NgbCarousel;

  public carouselImgList: Object;

  public paused = false;
  public unpauseOnArrow = false;
  public pauseOnIndicator = false;
  public pauseOnHover = true;

  constructor() { }

  ngOnInit() { }

  setCarouselImageArray(carouselObj: Object) {
    this.carouselImgList = carouselObj;
  }

  togglePaused() {
    if (this.paused) {
      this.carousel.cycle();
    } else {
      this.carousel.pause();
    }
    this.paused = !this.paused;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    if (this.unpauseOnArrow && slideEvent.paused &&
      (slideEvent.source === NgbSlideEventSource.ARROW_LEFT || slideEvent.source === NgbSlideEventSource.ARROW_RIGHT)) {
      this.togglePaused();
    }
    if (this.pauseOnIndicator && !slideEvent.paused && slideEvent.source === NgbSlideEventSource.INDICATOR) {
      this.togglePaused();
    }
  }

}
