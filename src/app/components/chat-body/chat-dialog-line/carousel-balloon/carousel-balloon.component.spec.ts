import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselBalloonComponent } from './carousel-balloon.component';

describe('CarouselBalloonComponent', () => {
  let component: CarouselBalloonComponent;
  let fixture: ComponentFixture<CarouselBalloonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselBalloonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselBalloonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
